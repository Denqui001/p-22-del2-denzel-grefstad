import random as rm
#Denzel Grefstad
def print_command_guide():
    mssg = """
Available commands:
 
    B - Converts a decimal number to binary - Task #2
    D - Converts a binary number to decimal - Task #3
    M - Math practice for multiplications between 0 and 10 - Task #1
    H - Prints this menu.
    Q - Quit, exits this program.
    """
    print(mssg)
    
    
def main():
    print("Welcome to the main menu of this programming task.")
    print_command_guide()
    running = True
   
    
    while running:
        choise = input("What would you like to do?\n\t> ").upper()
        
        
        match choise:
            case "B":
                #Oppgave 1! - Definer/lag funksjonen to_binary()
                print("Binary it is!!")
                to_binary()
                
            case "D":
                #Oppgave 2! - Definer/lag funksjonen to_decimal()
                print("Decimal! you got it!!")
                to_decimal()
                
            case "M":
                #Oppgave 3! - Definer/lag funksjonen math_practice()
                print("Multiplication practice!!")
                multiplication_practice()
                
            case "H":
                print_command_guide()
            case "Q":
                running = False  
                exit()  
            case _:
                print("No such command, enter 'H' for help.")      

def to_binary():
    while True:
        try:
            user_inp = int(input(" Skrvi en heltall: "))
            binary = format(user_inp, 'b')
        except ValueError:
            print("Skriv et heltall!")
            continue
        else:
            print(binary)

def to_decimal():
   while True:
    try:
        xnum = 2
        num = int(input("Input binary value: "), 2)
        print("num (decimal format):", num)
        print("num (binary format):", bin(num))  
    except ValueError:
        print("Please input only binary value...")
        print("This is a binary: ", bin(xnum),)
        print(" This is also binary 10101")

def multiplication_practice():
    while True:
        try:
            a = rm.randint(0,10)
            b = rm.randint(0,10)
            print("Hva er ",a,"*",b, " =",)
            x = a *b
            user_inp = int(input("Svar: "))
        except ValueError:
            print(" Skrive et heltall!")
            continue
        
        if user_inp != x:
            print(" That is wrong ")
            print(x, "is the correct answer!")
            continue
        elif user_inp in [x]:
                print ("correct!")
                c = input(" X to exit or a to continue").upper()
                if c in ['X']:
                    main()
                else:
                    continue

main()
